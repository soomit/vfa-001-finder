
/*
 * GET home page.
 */
exports.index = function(req, res){
	res.render('01');
};

//search page
exports.search = function(req, res){
	var user_search_value = req.param('slocation', '');
	var search_value = user_search_value + ' Switzerland'; // focus on Switzerland
	var search_loc;

	var gm = require('googlemaps');
	var Location = require('../models/location.js')();

	// search even if the user entered an empty string, to retrieve the coordinates for Switzerland (to center the map)
	gm.geocode(search_value, function(err, data) {
		if (data) {
      debugger;
			search_loc = data.results[0].geometry.location;

			// if the user entered an empty string - display all locations
			var search_conditions = {loc: {$exists: true}}; //geocoding does not work on all entries.
      
			if(user_search_value.length > 0) {
				// search in a radius of 25km
				// for a clarification of the 111.12 constant please see the following links:
				// http://www.mongodb.org/display/DOCS/Geospatial+Indexing
				// http://stackoverflow.com/questions/7837731/units-to-use-for-maxdistance-and-mongodb
				search_conditions = {loc : { $near : [search_loc.lng, search_loc.lat], $maxDistance: 25 / 111.12 }};
			}

			Location.find(
				search_conditions,
				function(err, locs) {
					// show them on the map
					res.render( req.url.indexOf('finder') > 0 ? 'finder' : '02',
						{
							slocation: user_search_value,
							locations: locs,
							searched_geo: search_loc,
							zoom: ((user_search_value.length > 0) ? 10 : 7)
						}
					);
				}
			);
		} else {
      //maybe we should also render when nothing is found..
      res.render( req.url.indexOf('finder') > 0 ? 'finder' : '02', {
          slocation: user_search_value,
          locations: [],
          searched_geo: {lat: 47.0, lng: 8.0}, //switzerland
          zoom: 7 
      });
    }
	});
};

// paypal return url
exports.paypal_done = function(req, res){
	var info = req.param("info");
	if(typeof(info) !== "undefined") {
		
    console.log("sending email");
    console.log(info);
		
		var email = require('mailer');

		email.send({
			ssl: true,
			host : process.env.MAILGUN_SMTP_SERVER,
			port : process.env.MAILGUN_SMTP_PORT,
			domain : "app4706624.mailgun.org",
			to :   process.env.MAIL_TO || "cedric.reginster@6grad.ch",
			from : process.env.MAIL_FROM || "donation@6grad.ch",
			subject : process.env.MAIL_SUBJECT || "new donation",
			template : "./views/email",
			data : info,
			authentication : "login",
			username : process.env.MAILGUN_SMTP_LOGIN,
			password : process.env.MAILGUN_SMTP_PASSWORD,
			debug: false
		},

		function(err, result){
			if(err){ console.log(err.stack); }
		});
	}
	
	res.render('paypal_done', {layout: false});
};
