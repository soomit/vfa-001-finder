;(function(global, $) {

  var app = global.app = {};

  app.donation_methods = function () {
    $('input[type=radio]').prettyCheckboxes();
    $('input[type=checkbox]').prettyCheckboxes();

    $('#confirmation').change(function() {
      if(this.checked) {
        $('#confirmationid').fadeIn();
      }else{
        $('#confirmationid').fadeOut();
      }
    });

    $('#inputdonationvalue').bind('click', function() {
      $('#radiovalue + label').click();
      $(this).focus();
    });

    $('#footer form').submit(function() {
      // validate the donation amount
      var donationvalue = $('input[name=donationvalue]:checked').val();
      switch(donationvalue){
        case "50": $("#footer form input[name=amount]").val("50");break;
        case "100": $("#footer form input[name=amount]").val("100");break;
        case "text":
                    var donation_amount = parseInt($("#inputdonationvalue").val());
                    if  ( donation_amount > 0 ) {
                      $("#footer form input[name=amount]").val(donation_amount);
                    } else {
                      $('#inputdonationvalue').focus();
                      alert('Bitte einen gültigen Betrag eingeben!');
                      return false;
                    }
                    break;
      }

      // validation for in case confirmation is required
      if ( $('#confirmationid:visible').length > 0 ) {
        var error_found = false;
        var error_message = 'Bitte alle Eingabefelder ausfüllen!';
        $('#confirmationid input[type=text]').each(function() {
          if (error_found) {
            return;
          }

          var val = $(this).val();

          // all are mandatory
          if ( val == '' || val == $(this).attr('placeholder') ) {
            error_found = true;
          }

          //valid zip code
          if ( $(this).attr('id') == 'plz' && (isNaN(val) || parseInt(val) < 1000 || parseInt(val) > 99999 ) ) {
            error_message = 'Bitte eine gültige PLZ eingeben';
            error_found = true;
          }
        });

        if ( error_found ) {
          alert(error_message);
          return false;
        }
      }

      if($('#confirmation').is(':checked')) {
        $('#footer form input[name=info]').val($('#confirmationform').serialize());
      }

      // redirect to the thank you page
      setTimeout(function() { window.location = "/05"; }, 1000);

    });

  };

})(this, jQuery);
