(function (global) {

  //expose us
  var $6 = global.$6 = {};

  var $dialog = $('#dialog');
  //init dialog
  $dialog.hide();
  $dialog.click(function() { $dialog.hide(); });

  $6.dialog = function (text) {
    $dialog.find('#dialog-inner').html(text);
    $dialog.show();
  };
})(this);
