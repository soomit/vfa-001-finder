/*
 * Import "Gathering Places" into Locations collection
 */

var request = require('request');
var csv = require('csv');
var mongoose = require('mongoose');
var gm = require('googlemaps');
var Location = require('./models/location')(mongoose);

//csv format
var DATA_URL = 'https://docs.google.com/spreadsheet/pub?key=0Ag_Fd79Q6hpvdGpfYlBZUUFtUF9qY2dUWmFhNDkwcmc&single=true&gid=0&output=csv';

//open db connection
mongoose.connect(process.env.DB_URL || 'mongodb://localhost/bikes4africa');

var update_geo = function () {
  // read all locations
  Location.find({}, function (err, locations) {
    var timer = 0;

    // iterate over them
    locations.forEach(function(location, idx) {
      var complete_address = location.address + ', ' + location.zip + ' ' + location.city;

      (function(complete_address, location) {
        setTimeout(function() {
          gm.geocode(complete_address, function(err, data) {

            if (idx === locations.length - 1) {
              mongoose.disconnect(function () {
                console.log('DONE');
              });
            }

            if (err) {
              return console.log('ERROR: ' + err);
            }

            if (data && data.results.length > 0) {
              var loc = data.results[0].geometry.location;

              console.log('got location [%s, %s] for address: %s', loc.lat, loc.lng, complete_address);

              location.loc = loc;
              location.location_type = data.results[0].geometry.location_type;
              location.save();
            } else {
              console.log('OOOPS: no location data found for ', complete_address);
            }
          });
        }, timer);

      }(complete_address, location));

      // do not stress google
      timer += 1000;
    });

  }, true);

};

request(DATA_URL, function (err, response, body) {

  if (err) {
    throw err;
  } else if (response.statusCode !== 200) {
    throw new Error('invalid response code: ' + response.statusCode + ', body: ' + body);
  } else {

    //flush before we insert
    Location.remove({}, function (err) {
      
      if (err) throw err;
      
      csv()
        .from(body)
        .on('data', function (data, i) {
          //skip header
          if (i === 0) return;

          var loc = new Location({
              name: data[0]
            , zip: data[1]
            , city: data[2]
            , address: data[3]
            , open: data[4]
            , contact: data[5]
            , pinColor: data[6]
            , text: data[7]
          });

          loc.save(function (err) {
            if (err) throw err;
          });
        })
        .on('end', function (count) {
          console.log('imported %s Places', count - 1);
          console.log('looking up locations using geocoding service from google..');
          //update geo locations
          setTimeout(update_geo, 3000);
        });
    });
  }
});
