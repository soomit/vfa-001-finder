var express = require('express')
  , mongoose = require('mongoose')
  , MongoStore = require('connect-mongo')(express)
  , Fybrid = require('fybrid')
  , util = require('util')
  , qs = require('querystring')
  , routes = require('./routes');

var app = module.exports = express.createServer();
var fybrid = new Fybrid({conf: __dirname + '/conf/fb.js'});

// Configuration
app.configure(function(){
	app.use(express.static(__dirname + '/public'));
  app.use(express.bodyParser());
	app.use(express.methodOverride());
  app.use(express.cookieParser());
  app.use(express.session({
    secret: process.env.APP_SECRET,
    store: new MongoStore({ url: process.env.MONGOLAB_URI })
  }));
  app.use(fybrid.mw());
	app.use(app.router);
	app.set('view engine','ejs');
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// open db connection
mongoose.connect(process.env.MONGOLAB_URI || 'mongodb://localhost/bikes4africa/');

// init Models ( use them later with require('models/location')() for ex.)
require('./models/location')(mongoose);

// main page
app.all('/', routes.index);

// search results/page 2
app.get('/s', routes.search);

// page 3
app.get('/03', function(req, res) {
  res.render('03', req.query );
});

// only the finder (google maps + search field)
app.get('/finder', function (req, res) {
  res.render('finder', {
    slocation: '',
    locations: [],
    searched_geo: { lat: 46.8637, lng: 8.1028},
    zoom: 7
  });
});
app.post('/finder', routes.search);

// donation form
// another entry point, therefore post for fb
app.all('/04', function(req, res) {
  req.query.return_path = req.headers.host;
  res.render('04', req.query );
});

app.post('/paypal', function (req, res) {
  
  var info = req.param('info', false);
  var amount = req.param('amount', '1.00');
  var returnUrl = 'https://' + req.headers.host + '/paypal_done' + ( info ? '?' + info : '' );

  console.log('loading paypal. amount %s, returnUrl %s', amount, returnUrl);

  res.render('paypal', {layout: false, returnUrl: returnUrl, amount: amount});
});

// thank you
app.get('/05', function(req, res) {
    res.render('05');
  });

// paypal return url
app.all('/paypal_done', routes.paypal_done);

app.listen(process.env.PORT || 3000, function(){
  console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});
