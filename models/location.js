//cache
var model;

var Location = module.exports = function (mongoose) {

  if (!mongoose) {
    if (model) {
      return model;
    } else {
      throw new Error('Model not initialized!');
    }
  }

  var Schema = mongoose.Schema;

	var LocationSchema = new Schema({
		name: String,
		zip: Number,
		city: String,
		address: String,
		open: String,
		contact: String,
    pinColor: String,
    text: String,
		loc: { type: { lng: Number, lat: Number }, index: '2d' },
		location_type: String
	});
	
  return (model = mongoose.model('Location', LocationSchema));

};
