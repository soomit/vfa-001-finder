var fb = module.exports = {
  id: process.env.FACEBOOK_APPID,
  secret: process.env.FACEBOOK_APPSECRET,
  links: {
    finder: process.env.FEED_LINK_FINDER || 'env.FEED_LINK_FINDER',
    donate: process.env.FEED_LINK_DONATE || 'env.FEED_LINK_DONATE'
  },
  dialogs: {
    feedFinder: function () {
      //Remember you are in the browser here
      return {
        method: 'feed',
        name: 'Möchtest du dein altes Velo für Afrika spenden?',
        picture: 'http://' + location.host + '/feedFinder.jpg',
        link: fb.links.finder,
        caption: 'Facebook.com/VelosfuerAfrika',
        description: 'Finde jetzt die nächste Sammelstelle in deiner Umgebung!'
      };
    },
    feedDonate: function () {
      return {
        method: 'feed',
        name: 'Ich unterstütze Velos für Afrika!',
        picture: 'http://' + location.host + '/feedDonate.jpg',
        link: fb.links.donate,
        caption: 'Facebook.com/VelosfuerAfrika',
        description: 'Unterstütze jetzt Velos für Afrika mit deiner Spende direkt über Facebook!'
      };
    }
  }
};
